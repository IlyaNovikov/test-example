import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SortService {

  constructor() {
  }

  customSort(array: string[], s: string[]) {
    return array.sort((x1, x2) => {
      let i1 = s.indexOf(x1)
      let i2 = s.indexOf(x2)
      return i1 < 0 ? 1 : i2 < 0 ? -1 : i1 - i2;
    })
  }
}
