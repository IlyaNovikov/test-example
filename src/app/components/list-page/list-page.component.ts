import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {IParams, IUser} from '../../types/user.interface'
import {UserService} from "../../api/user.service";
import {Subscription} from "rxjs";
import {SortService} from "../../services/sort.service";


@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit, OnDestroy {

  aSub: Subscription | undefined;
  bSub: Subscription | undefined;

  loading = false;
  selectedType = ''
  users: IUser[] = [];
  types: string[] = [];

  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,
              private sortService: SortService) {
  }

  ngOnInit(): void {
    this.render()
  }

  render() {
    this.loading = true;
    let paramsTypes: IParams | any = {};
    this.aSub = this.route.queryParams.subscribe((params: IParams) => {
        paramsTypes = params;
      }
    )
    this.bSub = this.userService.getAllUsers()
      .subscribe(users => {
        let types = [...new Set(users.map(user => user.type))];
        this.types = this.sortService.customSort(types, ["income", "outcome", "loan", "investment"])
        this.selectedType = this.types[+paramsTypes.tab]
        this.users = users.filter(user => user.type === this.selectedType)
        this.loading = false;
      })
  }

  goNextTab(type: string) {
    this.router.navigate(['/navigator'], {queryParams: {tab: this.types.indexOf(type)}})
    this.render()
  }

  ngOnDestroy() {
    this.aSub?.unsubscribe();
    this.bSub?.unsubscribe();
  }
}
