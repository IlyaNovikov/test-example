import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from "../../api/user.service";
import {Subscription} from "rxjs";
import {SortService} from "../../services/sort.service";
import {ITypes, IUser} from "../../types/user.interface";


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit, OnDestroy {
  loading = false;
  totalCount: number = 0
  transactions: ITypes[] = []
  aSub: Subscription | undefined;
  constructor(private userService: UserService, private sortService: SortService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.aSub = this.userService.getAllUsers()
      .subscribe(users => {
        let types = [...new Set(users.map(user => user.type))];
        let newTypes = this.sortService.customSort(types, ["income", "outcome", "loan", "investment"])
        this.totalCount = users.length
        newTypes.forEach((type:string) => {
          let usersLength = users.filter((user: IUser) => user.type === type).length;
          let indexOfType = newTypes.indexOf(type);
          this.transactions.push({
            name: type,
            count: usersLength,
            index: indexOfType
          })
        })
        this.loading = false;
      })
  }

  ngOnDestroy() {
    this.aSub?.unsubscribe()
  }
}
