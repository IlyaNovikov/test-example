export interface IUserName {
  first: string,
  last: string
}

export interface IParams {
  tab?: string,
}


export interface IUser {
  address: string,
  amount: number,
  company: string,
  email: string,
  name: IUserName,
  phone: string,
  type: "outcome" | "income" | "loan" | "investment",
  _id: string,
}

export interface ITypes {
  name: string,
  count: number,
  index: number
}
