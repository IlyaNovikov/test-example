import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SummaryComponent} from "./components/summary/summary.component";
import {ListPageComponent} from "./components/list-page/list-page.component";


//http://localhost:4200/ -> SummaryComponent
//http://localhost:4200/navigator -> ListPageComponent

const routes: Routes = [
  {path: '', component: SummaryComponent},
  {path: 'navigator', component: ListPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
