import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ListPageComponent } from './components/list-page/list-page.component';
import { SummaryComponent } from './components/summary/summary.component';
import {HttpClientModule} from "@angular/common/http";
import { LoaderComponent } from './UI/loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListPageComponent,
    SummaryComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
